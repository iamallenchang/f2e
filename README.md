# F2E

### Pomodoro

- 未完成部分  
*[TODO] analytics page*  
*[TODO] ringtones page*  
*[TODO] todolist page*


### Freecell

- 未完成部分  
*[TODO] study ts use vuedraggable*  
*[TODO] 遞迴部分*  
*[TODO] main style*  


### MP3player

- 未完成部分  
*[TODO] mp3 功能部分*  
*[TODO] swiper 功能部分*  


### Payment

- 未完成部分  
*[TODO] 細部 css 調整, e.g. btn狀態*   


### 90sec Game

- 未完成部分  
*[TODO] study use phaser*  
*[TODO] 角色功能*  
*[TODO] 怪物功能、位置*
*[TODO] 計時功能、分數、生命、暫停等功能*  


## Hotel

- 未完成部分  
*[NOTE] contact & foryou 無頁面*  
*[TODO] room form 部分*  
*[TODO] api post*


## Chatroom

- 未完成部分  
*[TODO] rooms emoji & file部分*  
*[TODO] 說明部分*  


## Clouddrive

- 未完成部分  
*[TODO] firebase，相關功能部分*  
*[TODO] 頁面css功能*  


## Markdown

- 未完成部分  
*[TODO] 功能部分，切換 css、sort、新增未完成*  
*[TODO] 筆記部分功能皆未完成*  